#include <gtk/gtk.h>
#include "projects.h"
#include "widgets.h"

app_widgets	*widgets;

int main(int argc, char *argv[])
{
	GtkBuilder	*builder;
    GtkWidget   *window;
	widgets = g_slice_new(app_widgets);
 
    gtk_init(&argc, &argv);
 
    builder = gtk_builder_new();
    gtk_builder_add_from_file (builder, "ui.glade", NULL);
 
    window = GTK_WIDGET(gtk_builder_get_object(builder, "window1"));

	widgets->tv_output = GTK_WIDGET(gtk_builder_get_object(builder, "tv_output"));    
	widgets->aboutdialog1 = GTK_DIALOG(gtk_builder_get_object(builder, "aboutdialog1"));    
	widgets->applicationwindow1 = GTK_WIDGET(gtk_builder_get_object(builder, "applicationwindow1"));    
    gtk_builder_connect_signals(builder, widgets);
 
    g_object_unref(builder);
 
    gtk_widget_show(window);                
    gtk_main();

	g_slice_free(app_widgets, widgets);
 
    return 0;
}

// called when window is closed
void quit()
{
    gtk_main_quit();
}

void sig_about(GtkButton *button, app_widgets *app_wdgts)
{
	printf("About]n\n");
	gtk_dialog_run(app_wdgts->aboutdialog1);
}

void sig_new_file(GtkButton *button, app_widgets *app_wdgts)
{
	GtkWidget *view = app_wdgts->tv_output;
	GtkTextBuffer* buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(view));
	GtkTextIter end;
    gtk_text_buffer_get_end_iter(buffer, &end);
	gtk_text_buffer_insert(buffer, &end, "Hello ubuntuforums", -1);
	printf("New file\n");	
}
