#include "projects.h"

extern app_widgets *widgets;

void sig_new_project(GtkButton *button, app_widgets *app_wdgts)
{
    gtk_widget_show(app_wdgts->applicationwindow1);
    printf("New project\n");
}

void sig_project_cancel(GtkButton *button, app_widgets *app_wdgts)
{
    gtk_widget_hide(app_wdgts->applicationwindow1);
	printf("sig_project_cancel()\n");
}

void sig_project_create(GtkButton *button, app_widgets *app_wdgts)
{
	printf("sig_project_create()\n");
}

