#ifndef __projects_h__
#define __projects_h__

#include <gtk/gtk.h>
#include "widgets.h"

void sig_new_project(GtkButton *button, app_widgets *app_wdgts);

#endif
