# change application name here (executable output name)
TARGET=editor
CC=gcc
DEBUG=-g
OPT=-O0
WARN=-Wall
 
PTHREAD=-pthread
 
CCFLAGS=$(DEBUG) $(OPT) $(WARN) $(PTHREAD) -pipe -rdynamic
 
GTKLIB=`pkg-config --cflags --libs gtk+-3.0`
 
LD=gcc
LDFLAGS=$(PTHREAD) $(GTKLIB) -export-dynamic
 
OBJS=main.o projects.o
 
all: $(OBJS)
	$(LD) -o $(TARGET) $(OBJS) $(LDFLAGS)
    
main.o: src/main.c
	$(CC) -c $(CCFLAGS) src/main.c $(GTKLIB) -o main.o

projects.o: src/projects.c
	$(CC) -c $(CCFLAGS) src/projects.c $(GTKLIB) -o projects.o
    
clean:
	rm -f *.o $(TARGET)
